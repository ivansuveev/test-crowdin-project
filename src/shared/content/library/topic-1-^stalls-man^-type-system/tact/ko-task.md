Add variables to the code:
* An unsigned 16-bit int variable `i`, with a value of 10
* A boolean variable `b` with initialization value `true` in `init()` function.
