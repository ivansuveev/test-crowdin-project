**Int** - Enteros con signo de 257 bits
```
i1: Int;
i2: Int = 3001;
```

Para reducir el coste del almacenamiento, a menudo se utilizan representaciones más pequeñas, por ejemplo:
```
i3: Int as int8 = 100;
i4: Int as uint32 = 10;
```

Es conveniente inicializar variables usando `init()` al desplegar un contrato:
```
i1: Int;
init() {
    self.i1 = 100;
}
```

**Bool** - booleano clásico con valores verdadero/falsos.
```
b1: Bola = verdadera;
```

**Dirección** - Dirección estándar.
```
a1: Dirección = address("EQCD39VS5jcptHL8vMjEXrzGaRcCVYto7HUn4bpAOg8xqB2N");
```

**String** - tipo que representa cadenas de texto en la máquina virtual TON.
```
s1: String = "Hola mundo";
let i1: Int = -12345;
let i2: Int = 6780000000; // monedas = ton("6. 8")
self.s3 = i1.toString(); // Convierte el valor Int a String
mismo. 4 = i1.toFloatString(3); // Convierte el valor de float fijo que representa como Int a String
mismo. 5 = i2.toCoinsString(); // Convierte el valor de la int nanoToncoin automáticamente a la cadena decimal número de Toncoin
```

**StringBuilder** - tipo de ayudante que te permite concatenar cadenas de manera eficiente con gas
```
let sb1: StringBuilder = beginString(); //Create new StringBuilder
sb. ppend("Hola mundo"); //Añadido a la cadena vacía `Hola mundo`
self.s1 = sb1.toString(); // Convierte StringBuilder a String
```

**Estructura** - estructura de datos regular. Las estructuras pueden ser anidadas.
```
struct Point {
    x: Int;
    y: Int;
}
```

**Mensaje** - difiere de la estructura de los temas en que los mensajes tienen un identificador de encabezado. Esto permite que los mensajes se utilicen más tarde en los receptores.

**Mapa** - par <key, value>.
- Tipos de clave posibles: Int, Dirección
- Tipos de valor posibles: Int, Bool, Célula, Dirección, Estructural/Mensaje
```
m1: mapear<Int, Int>;
init() {
    self.m1.set(100, 1000);
}
```
- Puede obtener un valor por clave usando el método `get()`

**Contrato**. Los contratos son la entrada principal de un contrato inteligente en la cadena de bloques TON. Contiene todas las funciones, getters y receptores de un contrato.

[**Rasgos**.](https://docs.tact-lang.org/language/guides/types#traits) Trait define funciones, receptores y campos requeridos. El rasgo es como las clases abstractas, pero no define cómo y dónde deben almacenarse los campos. **Corte**, **Celda**, **Constructor** - primitivo de nivel bajo de TON VM.

**Opciones**

Si añade un `?` al final del tipo, el campo se vuelve opcional. Pero en este caso, es necesario saber con certeza que la variable no es nula y añadir `!` al final de la variable en caso de usar su valor para informar al compilador.
```
struct Point {
    x: Int;
    y: Int?;
}
p1: Point;
...
devolver p1.y!!;
```

**Constantes:**
```
const StateUnpaid: Int = 0;
```

También existen constantes [virtuales y abstractas](https://docs.tact-lang.org/language/guides/constants#virtual-and-abstract-constants), que pueden declararse en los rasgos y modificarse en los contratos.
