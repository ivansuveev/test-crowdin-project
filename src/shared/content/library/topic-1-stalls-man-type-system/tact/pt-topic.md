**Int** - inteiros assinados em 257 bits
```
i1: Int;
i2: Int = 3001;
```

Para reduzir o custo do armazenamento, são frequentemente utilizadas representações menores, por exemplo:
```
i3: Int as int8 = 100;
i4: Int as uint32 = 10;
```

É conveniente inicializar variáveis usando `init()` ao implantar um contrato:
```
i1: Int;
init() {
    self.i1 = 100;
}
```

**Bool** - valor booleano clássico com valores verdadeiros/falsos.
```
b1: Bool = verdadeiro;
```

**Endereço** - endereço padrão.
```
a1: Endereço = endereço("EQCD39VS5jcptHL8vMjEXrzGaRcCVYto7HUn4bpAOg8xqB2N");
```

**String** - tipo que representa strings de texto no TON VM.
```
s1: String = "olá mundo";
let i1: Int = -12345;
let i2: Int = 6780000000; // coins = ton("6. 8")
self.s3 = i1.toString(); // Converte o valor Int para string
a si mesmo. 4 = i1.toFloatString(3); // Converte um valor fixo de Float que representava como Int to String
5 = i2.toCoinsString(); // Converte o valor nanoToncoin Int para o número flutuante de Toncoin
```

**StringBuilder** - tipo de ajuda que permite concatenar strings de forma eficiente ao gás
```
let sb1: StringBuilder = beginString(); //Create new StringBuilder
vazio. ppend("Olá mundo"); //Adicionado à string vazia `Hello world`
self.s1 = sb1.toString(); // Converte StringBuilder para String
```

**Estrutura** - estrutura regular de dados. Estruturas podem ser aninhadas.
```
struct Point {
    x: Int;
    y: Int;
}
```

**Mensagem** - difere da estrutura de tópicos nas mensagens que possuem um identificador de cabeçalho. Isso permite que as mensagens sejam usadas mais tarde nos destinatários.

**Mapa** - parear <key, value>.
- Possíveis tipos de chaves: Int, Address
- Possíveis tipos de valor: Int, Bool, Celula, Endereço, Estrutura/Mensagem
```
m1: mapa<Int, Int>;
init() {
    self.m1.set(100, 1000);
}
```
- Você pode obter um valor pela chave usando o método `get()`

**Contrato**. Os contratos são a entrada principal de um contrato inteligente na blockchain TON. Ele possui todas as funções, contadores e receptores de um contrato.

[**Características**.](https://docs.tact-lang.org/language/guides/types#traits) Característica define funções, receptores e campos obrigatórios. Característica é como classe abstrata, mas não define como e onde os campos devem ser armazenados. **Slice**, **Cell**, **Builder** - nível primitivo da TON VM.

**Opcionais**

Caso você adicione um `?` no final do tipo, o campo torna-se opcional. Mas, nesse caso, é necessário saber com certeza que a variável não é nula e adicionar `!` no final da variável no caso de usar seu valor para informar ao compilador.
```
struct Point {
    x: Int;
    y: Int?;
}
p1: Ponto;
...
retornar p1.y!!;
```

**Constantes:**
```
const StateUnpaid: Int = 0;
```

Também existem [constantes virtuais e abstratas](https://docs.tact-lang.org/language/guides/constants#virtual-and-abstract-constants), que podem ser declaradas nas características e modificadas nos contratos.
