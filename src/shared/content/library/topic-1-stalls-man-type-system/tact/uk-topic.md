**Int** - 257 бітове ціле число
```
i1: Int;
i2: Int = 3001;
```

Щоб зменшити вартість пам'яті, часто використовуються менші представництва, наприклад:
```
i3: Int як int8 = 100;
i4: Int як uint32 = 10;
```

It’s convenient to initialize variables using `init()` when deploying a contract:
```
i1: Int;
init() {
    self.i1 = 100;
}
```

**Бул** - класичний логічний з значеннями true/false s.
```
b1: Вище = правда;
```

**адреса** - резервна адреса.
```
a1: Address = address("EQCD39VS5jcptHL8vMjEXrzGaRcCVYto7HUn4bpAOg8xqB2N");
```

**String** - тип, який представляє текстові рядки в TON VM.
```
s1: String = "привіт світ";
let i1: Int = -12345;
Нехай i2: Int = 6780000000; // монети = тон("6. 8")
self.s3 = i1.toString(); // Перетворює Int значення в String
self. 4 = i1.toFloatString(3); // Перетворює фіксоване значення Float, яке представлено як Int в String
self. 5 = i2.toCoinsString(); // Converts nanoToncoin Int значення я в String float кількість Toncoin
```

**StringBuilder** - тип помічника, який дозволяє вам об'єднати рядки ефективним способом роботи бензину
```
let sb1: StringBuilder = beginString(); //Create new empty StringBuilder
sb. ppend("Hello world"); //added to the empty string `Hello world`
self.s1 = sb1.toString(); // Перетворює StringBuilder в String
```

**Структура** - звичайна структура даних. Структури можуть бути вкладені.
```
struct Point {
    x: Int;
    y: Int;
}
```

**Повідомлення** - відрізняється від структури тем в повідомленнях з ідентифікатором заголовка. Це дозволяє повідомленням використовувати пізніше в ресиверах.

**Map** - pair <key, value>.
- Можливі типи ключів: Int, адреса
- Можливі типи значень: Int, Bool, Cell, Address, Struct/Message
```
m1: map<Int, Int>;
init() {
    self.m1.set(100, 1000);
}
```
- Ви можете отримати значення за ключем за допомогою методу `get()`

**Договір**. Контракти - основний запис розумного договору на TON blockchain. Він тримає всі функції, отримувачі і одержувачі договору.

[**Traits**.](https://docs.tact-lang.org/language/guides/types#traits) Trait defines functions, receivers and required fields. Риса як абстрактні класи, але вона не визначає, як і де зберігаються поля. **Slice**, **Cell**, **Builder** - примітивне значення TON VM.

**Опціональні**

Якщо ви додасте `?` в кінці типу, поле стає необов'язковим. Але у цьому випадку необхідно знати, що змінна не null і додати `!` у кінці змінної у випадку використання його значення, щоб інформувати компілятор.
```
struct Point {
    x: Int;
    y: Int?;
}
p1: Point;
...
повернути p1.y!!;
```

**Стани:**
```
const StateUnpaid: Int = 0;
```

Також є [віртуальні і абстрактні константи](https://docs.tact-lang.org/language/guides/constants#virtual-and-abstract-constants), які можуть бути оголошені в рисах і змінені в контрактах.
