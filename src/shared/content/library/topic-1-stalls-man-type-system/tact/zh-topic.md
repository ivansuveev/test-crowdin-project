**Int** - 257 位符号整数
```
i1: 内部;
i2: Int = 3001;
```

为了降低储存成本，常常采用较小的表述形式，例如：
```
i3: Int as int8 = 100;
i4: Int as uint32 = 10;
```

在部署合约时使用 `init()` 初始化变量是方便的：
```
i1: Int;
init() lew
    self.i1 = 100;
}
```

**Bool** - classical boolean with true/false values.
```
b1：布尔=真；
```

**地址** - 标准版地址。
```
a1：地址 = 地址("EQCD39VS5jcptHL8vMjEXrzGaRcVYto7HUn4bpAOg8xqB2N")；
```

**字符串** - 以TON VM表示文本字符串的类型。
```
s1: String = "hello world";
let i1: Intt = -12345;
let i2: Int = 6780000000; // coins = ton("6. 8")
self.s3 = i1.toString(); // 将Int值转换为字符串
自己。 4 = i1.toFloatString(3)；// 将代表Int 的固定浮点数转换为字符串
自己。 5 = i2.toCoinsString(); // 将 nanoToncoin Int 值自身转换为 Toncoin 的字符串浮点数
```

**StringBuilder** - 能够让您以有效气体的方式连接字符串的助手类型
```
let sb1: StringBuilder = beginString(); //Create new bloth StringBuilder
sb. pend("Hello world")；/添加到空字符串 `Hello world`
self.s1 = sb1.toString(); // 转换StringBuilder 到字符串
```

**结构** - 常规数据结构。 结构可以嵌套。
```
struct Point Point v.
    x: 英寸;
    y: 英寸;
}
```

**消息** - 不同于主题结构，因为消息有标题标识符。 这将允许消息稍后在接收器中使用。

**地图** - 配对 <key, value>
- 可能的密钥类型: Int, 地址
- 可能的值类型：Int, Bool, Cell, Address, Struct/Message
```
m1: map<Int, Int>;
init() 然后再返回
    self.m1.set(100, 1000);
}
```
- 您可以使用方法 `get()` 获取一个值

**合同** 合同是TON区块链上智能合同的主要条目。 它拥有合同的所有功能、获取者和接受者。

[****特性。](https://docs.tact-lang.org/language/guides/types#traits) 特性定义函数、接受器和必填字段。 特性就像抽象类，但它没有定义字段必须存储的方式和位置。 **分割**, **单元**, **构建器** - 低水平原始TON VM

**可选的**

如果您添加了一个 `吗？` 在类型末尾，此字段变成可选的。 但在这种情况下，必须知道该变量不是空的并添加 `！` 在变量结尾处使用其值通知编译器。
```
struct Point Point v.
    x: Int;
    y: Int?;
}
p1: Point;
...
返回 p1.y!!;
```

**常量：**
```
const Status Unpaiid: Int = 0;
```

还有 [虚拟和抽象常量](https://docs.tact-lang.org/language/guides/constants#virtual-and-abstract-constants)，这些常量可以用特性声明并在合约中修改。
