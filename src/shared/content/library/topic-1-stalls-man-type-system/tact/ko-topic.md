**Int** - 257 bit signed integers
```
i1: Int;
i2: Int = 3001;
```

To reduce the cost of storage, smaller representations are often used, for example:
```
i3: Int as int8 = 100;
i4: Int as uint32 = 10;
```

It’s convenient to initialize variables using `init()` when deploying a contract:
```
i1: Int;
init() {
    self.i1 = 100;
}
```

**Bool** - classical boolean with true/false values.
```
b1: Bool = true;
```

**Address** - standart address.
```
a1: Address = address("EQCD39VS5jcptHL8vMjEXrzGaRcCVYto7HUn4bpAOg8xqB2N");
```

**String** - type that represents text strings in TON VM.
```
s1: String = "hello world";
let i1: Int = -12345;
let i2: Int = 6780000000; // coins = ton("6.78")
self.s3 = i1.toString(); // Converts Int value to String
self.s4 = i1.toFloatString(3); // Converts Fixed Float value that represented as Int to String
self.s5 = i2.toCoinsString(); // Converts nanoToncoin Int value self to String float number of Toncoin
```

**StringBuilder** - helper type that allows you to concatenate strings in gas-efficient way
```
let sb1: StringBuilder = beginString(); //Create new empty StringBuilder
sb.append("Hello world"); //Added to the empty string `Hello world`
self.s1 = sb1.toString(); // Converts StringBuilder to String
```

**Struct** - regular data structure. Structures can be nested.
```
struct Point {
    x: Int;
    y: Int;
}
```

**Message** - differs from the structure of topics in that messages have a header-identifier. This allows the messages to be used later in receivers.

**Map** - pair <key, value>.
- Possible key types: Int, Address
- Possible value types: Int, Bool, Cell, Address, Struct/Message
```
m1: map<Int, Int>;
init() {
    self.m1.set(100, 1000);
}
```
- You may get a value by key using the method `get()`

**Contract**. Contracts are the main entry of a smart contract on TON blockchain. It holds all functions, getters and receivers of a contract.

[**Traits**.](https://docs.tact-lang.org/language/guides/types#traits) Trait defines functions, receivers and required fields. Trait is like abstract classes, but it does not define how and where fields must be stored. **Slice**, **Cell**, **Builder** - low level primitive of TON VM.

**Optionals**

If you add a `?` at the end of the type, the field becomes optional. But in this case, it is necessary to know for sure that the variable is not null and add `!!` at the end of the variable in case of using its value to inform the compiler.
```
struct Point {
    x: Int;
    y: Int?;
}
p1: Point;
...
return p1.y!!;
```

**Constants:**
```
const StateUnpaid: Int = 0;
```

There are also [virtual and abstract constants](https://docs.tact-lang.org/language/guides/constants#virtual-and-abstract-constants), which can be declared in traits and modified in contracts.
